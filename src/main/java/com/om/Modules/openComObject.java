package com.om.Modules;

import org.springframework.stereotype.Repository;

/**
 * @author zhxia
 * @date 2020/11/5 16:36
 */
@Repository
public class openComObject {
    protected   String extOs_index;
    protected   String extOs_queryStr;
    protected   String businessOsv_index;
    protected   String businessOsv_queryStr;
    protected   String sigs_index;
    protected   String sigs_queryStr;
    protected   String users_index;
    protected   String users_queryStr;
    protected   String contributors_index;
    protected   String contributors_queryStr;
    protected  String noticeusers_index;
    protected String noticeusers_queryStr;
    protected String communitymembers_index;
    protected String communitymembers_queryStr;
    protected String GiteeAllIndex;
    protected String GiteeAll_qIssueStrBymil;
    protected String GiteeAllQueryAllstr;

    public String getGiteeAllQueryAllstr() {
        return GiteeAllQueryAllstr;
    }

    public void setGiteeAllQueryAllstr(String giteeAllQueryAllstr) {
        GiteeAllQueryAllstr = giteeAllQueryAllstr;
    }

    public String getGiteeAll_qIssueStrBymil() {
        return GiteeAll_qIssueStrBymil;
    }

    public void setGiteeAll_qIssueStrBymil(String giteeAll_qIssueStrBymil) {
        GiteeAll_qIssueStrBymil = giteeAll_qIssueStrBymil;
    }

    public String getGiteeAllIndex() {
        return GiteeAllIndex;
    }

    public void setGiteeAllIndex(String giteeAllIndex) {
        GiteeAllIndex = giteeAllIndex;
    }

    public String getExtOs_index() {
        return extOs_index;
    }

    public void setExtOs_index(String extOs_index) {
        this.extOs_index = extOs_index;
    }

    public String getExtOs_queryStr() {
        return extOs_queryStr;
    }

    public void setExtOs_queryStr(String extOs_queryStr) {
        this.extOs_queryStr = extOs_queryStr;
    }

    public String getSigs_index() {
        return sigs_index;
    }

    public void setSigs_index(String sigs_index) {
        this.sigs_index = sigs_index;
    }

    public String getSigs_queryStr() {
        return sigs_queryStr;
    }

    public void setSigs_queryStr(String sigs_queryStr) {
        this.sigs_queryStr = sigs_queryStr;
    }

    public String getUsers_index() {
        return users_index;
    }

    public void setUsers_index(String users_index) {
        this.users_index = users_index;
    }

    public String getUsers_queryStr() {
        return users_queryStr;
    }

    public void setUsers_queryStr(String users_queryStr) {
        this.users_queryStr = users_queryStr;
    }

    public String getContributors_index() {
        return contributors_index;
    }

    public void setContributors_index(String contributors_index) {
        this.contributors_index = contributors_index;
    }

    public String getContributors_queryStr() {
        return contributors_queryStr;
    }

    public void setContributors_queryStr(String contributors_queryStr) {
        this.contributors_queryStr = contributors_queryStr;
    }

    public String getNoticeusers_index() {
        return noticeusers_index;
    }

    public void setNoticeusers_index(String noticeusers_index) {
        this.noticeusers_index = noticeusers_index;
    }

    public String getNoticeusers_queryStr() {
        return noticeusers_queryStr;
    }

    public void setNoticeusers_queryStr(String noticeusers_queryStr) {
        this.noticeusers_queryStr = noticeusers_queryStr;
    }

    public String getBusinessOsv_index() {
        return businessOsv_index;
    }

    public void setBusinessOsv_index(String businessOsv_index) {
        this.businessOsv_index = businessOsv_index;
    }

    public String getBusinessOsv_queryStr() {
        return businessOsv_queryStr;
    }

    public void setBusinessOsv_queryStr(String businessOsv_queryStr) {
        this.businessOsv_queryStr = businessOsv_queryStr;
    }

    public String getCommunitymembers_index() {
        return communitymembers_index;
    }

    public void setCommunitymembers_index(String communitymembers_index) {
        this.communitymembers_index = communitymembers_index;
    }

    public String getCommunitymembers_queryStr() {
        return communitymembers_queryStr;
    }

    public void setCommunitymembers_queryStr(String communitymembers_queryStr) {
        this.communitymembers_queryStr = communitymembers_queryStr;
    }
}


