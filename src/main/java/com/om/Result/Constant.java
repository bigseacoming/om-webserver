package com.om.Result;

/**
 * @author xiazhonghai
 * @date 2021/2/1 18:44
 * @description:常量类
 */
public class Constant {
    public static final String openeuler="openeuler";
    public static final String opengauss="opengauss";
    public static final String openlookeng="openlookeng";
    public static final String mindspore="mindspore";
    public static final String individual="individual";
    public static final String organization="organization";
    public static final String allIssueCveStr="allIssueCveStr";
    public static final String allIssueResult="allIssueResult";
}
