package com.om.Vo;

/**
 * @author xiazhonghai
 * @date 2021/2/3 13:17
 * @description:
 */
public class ContributionResultVoPie {
    Double number;
    String name;

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
