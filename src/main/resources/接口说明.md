#### 查询接口

**请求格式：** http://192.168.0.1:8888/query/{item}?community={commname}

**item:**

|item参数名|描述|
|  ----  | ----|
| contributors  | 贡献者 |
| sigs  | sig组 |
| users  | 社区用户 |
| noticusers  | 参与活动人次 |
| modulenums  | 仓库软件 |
| businessosv  | 商业发行版 |
| communitymembers  | 社区会员 |
| all  | 所有数据 |

**community:**

|  community参数名   | 描述|
|  ----  | ----  |
| openEuler  | openEuler社区 |
| openGauss  | openGaussr社区 |
| openLookeng  | openLookeng社区 |

**example:**
```python
request:http://192.168.0.1:8888/query/users?community=openEuler
response:{"code":200,"data":{"users":12357},"msg":"OK"}
```

```python
request:http://192.168.0.1:8888/query/all?community=openEuler
response:{"code":200,"data":{"contributors":1636,"users":12357,"noticusers":480931,"sigs":73,"modulenums":7269},"msg":"OK"}
```



